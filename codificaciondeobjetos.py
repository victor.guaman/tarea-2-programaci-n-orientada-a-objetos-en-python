# Codificar el siguiente modelo de clases  en python, en un módulo llamado recursos.
#    Crear un programa en el cual se permita crear una actividad que contenga al menos 3 preguntas de
#    selección en base al modelo de clases codificado en el item 1 presentado al final
# todo lo creado .
class actividad:
    indentificador = str
    nombre = str
    preguntas = []

    def __init__(self, identificador, nombre, preguntas):
        self.identificador = identificador
        self.nombre = nombre
        self.preguntas = preguntas


mv = actividad('usuario', 'Juan', '¿cual es tu nombre?')
print(mv)
print('idenificador', mv.identificador)
print('nombre:', mv.nombre)
print('preguntas:', mv.preguntas)


# clase dos
class preguntaSeleccion:
    numero = 0
    texto = str
    opcion1 = str
    opcion2 = str
    opcion3 = str
    respuesta = str

    def __init__(self, numero, texto, opcion1, opcion2, opcion3, respuesta):
        self.numero = numero
        self.texto = texto
        self.opcion1 = opcion1
        self.opcion2 = opcion2
        self.opcion3 = opcion3
        self.respuesta = respuesta


pre = preguntaSeleccion(2, 'hola', '¿cual es tu nombre?', 'que edad tienes', 'donde estas', 'obtengo la respuesta')
print('numero:', pre.numero)
print('texto:', pre.texto)
print('opcion1:', pre.opcion1)
print('opcion2:', pre.opcion2)
print('opcion3:', pre.opcion3)
print('respuesta:', pre.respuesta)